#!/bin/sh
#------------------------------------------------------------------------------
#
# reposync.sh: artifact deployment / repository sync script for Drupal codebases
#
# This script commits any differences between the build artifact of a source
# Drupal codebase repository and a deployment repository.
#
# To work properly, this script must be run from within a cloned working tree of
# the source repository and a build artifact must exist in $UAQSRTOOLS_ARTIFACT_PATH.
#
# Required environment variables
# - UAQSRTOOLS_ARTIFACT_PATH The path of the build artifact of the source Drupal codebase (no trailing slash)
# - UAQSRTOOLS_TARGET_REPO_URL The URL of the target repository
#
# Optional environment variables (with empty or computed defaults).
# - UAQSRTOOLS_CLONE_PATH A permanent directory to clone the target repository into
# - UAQSRTOOLS_COMMIT_MSG The message (or template) to use when committing differences between repositories
# - UAQSRTOOLS_EXTRA_SYNC_PATHS Space-delimited list of additional paths within the source repository to sync
# - UAQSRTOOLS_TARGET_SUBDIR The name of subdirectory within the target repository to perform the sync against
#------------------------------------------------------------------------------

set -e

#------------------------------------------------------------------------------
# Optional environment variables with simple defaults.

# The domain name of the server hosting the source repository
: "${UAQSRTOOLS_SOURCE_GIT_SERVER:=bitbucket.org}"

# The username of the source repository owner
: "${UAQSRTOOLS_SOURCE_REPO_OWNER:=ua_drupal}"

# The name of the source repository
: "${UAQSRTOOLS_SOURCE_REPO_SLUG:=ua_quickstart}"

# The name of the target branch in the target repository
: "${UAQSRTOOLS_TARGET_BRANCH:=master}"

# Short Git remote name for the remote target repository
: "${UAQSRTOOLS_TARGET_REMOTE:=origin}"

#------------------------------------------------------------------------------
# Utility functions definitions.

errorexit () {
  echo "** $1." >&2
  exit 1
}

# Show progress on STDERR, unless explicitly quiet.
if [ -z "$UAQSRTOOLS_QUIET" ]; then
  logmessage () {
    echo "$1..." >&2
  }
  normalexit () {
    echo "$1." >&2
    exit 0
  }
else
  logmessage () {
    return
  }
  normalexit () {
    exit 0
  }
fi

#------------------------------------------------------------------------------
# Git commit message templates

makegitcommitmsg () {
  source_repo_path="${UAQSRTOOLS_SOURCE_REPO_OWNER}/${UAQSRTOOLS_SOURCE_REPO_SLUG}"
  if [ "$UAQSRTOOLS_SOURCE_GIT_SERVER" = "bitbucket.org" ]; then
    source_commit_ref="https://${UAQSRTOOLS_SOURCE_GIT_SERVER}/${source_repo_path}/commits/%h"
  else
    source_commit_ref="%h"
  fi
  UAQSRTOOLS_COMMIT_MSG=$(git log -1 --format=format:"%B(${source_commit_ref} committed by %an)")
}

#------------------------------------------------------------------------------
# Initial run-time error checking.

which rsync > /dev/null \
  || errorexit "Can't find the rsync command"
which git > /dev/null \
  || errorexit "Can't find the git command"
git status \
  || errorexit "This script must be run from within a cloned working tree of the source repository"
[ -n "$UAQSRTOOLS_TARGET_REPO_URL" ] \
  || errorexit "No target repository URL specified"
[ -n "$UAQSRTOOLS_ARTIFACT_PATH" ] \
  || errorexit "No path to the build artifact specified"
[ -d "$UAQSRTOOLS_ARTIFACT_PATH" ] \
  || errorexit "Can't find the build artifact path at ${UAQSRTOOLS_ARTIFACT_PATH}"

#------------------------------------------------------------------------------
#  Initial setup

[ -n "$UAQSRTOOLS_COMMIT_MSG" ] \
  || makegitcommitmsg
if [ -z "$UAQSRTOOLS_CLONE_PATH" ]; then
  UAQSRTOOLS_CLONE_PATH=$(mktemp -d -t "uaqsrtools_target_sync_XXXXXX") \
    || errorexit "Can't make a temporary target clone directory"
  trap 'rm -Rf "${UAQSRTOOLS_CLONE_PATH}"' EXIT TERM INT QUIT
fi
[ -d "$UAQSRTOOLS_CLONE_PATH" ] \
  || errorexit "No directory to hold the target repository clone"
if [ -z "$UAQSRTOOLS_TARGET_SUBDIR" ]; then
  target_sync_path="${UAQSRTOOLS_CLONE_PATH}"
else
  target_sync_path="${UAQSRTOOLS_CLONE_PATH}/${UAQSRTOOLS_TARGET_SUBDIR}"
fi

#------------------------------------------------------------------------------
#  Target repository manipulations

logmessage "Cloning the target repository"
git clone "$UAQSRTOOLS_TARGET_REPO_URL" --branch "$UAQSRTOOLS_TARGET_BRANCH" "$UAQSRTOOLS_CLONE_PATH" \
  || errorexit "Failed to clone the ${UAQSRTOOLS_TARGET_BRANCH} from the ${UAQSRTOOLS_TARGET_REPO_URL} repository into ${UAQSRTOOLS_CLONE_PATH}"

logmessage "Syncing Drupal core and profile changes"
rsync -r --delete \
  --exclude='.htaccess' \
  --exclude='.git' \
  --exclude='.gitignore' \
  --exclude='robots.txt' \
  --exclude='/sites' \
  "$UAQSRTOOLS_ARTIFACT_PATH"/* "$target_sync_path" \
  || errorexit "rsync crashed with status ${?} on the build artifact from ${UAQSRTOOLS_ARTIFACT_PATH}"
for extra_path in $UAQSRTOOLS_EXTRA_SYNC_PATHS ; do
  source_extra_path="${UAQSRTOOLS_ARTIFACT_PATH}/${extra_path}"
  target_extra_path="${target_sync_path}/${extra_path}"
  logmessage "Syncing changes from the additional directory ${source_extra_path}"
  [ -d "$source_extra_path" ] \
    || errorexit "Can't find the extra sync directory $source_extra_path"
  rsync -r --delete "$source_extra_path"/* "$target_extra_path" \
    || errorexit "rsync crashed with status ${?} processing ${source_extra_path} -> ${target_extra_path}"
done

logmessage "Commiting and pushing changes to the target repository"
cd "$UAQSRTOOLS_CLONE_PATH"
git add -A . \
  || errorexit "Can't add the target clone changes"
git commit --allow-empty -am "$UAQSRTOOLS_COMMIT_MSG" \
  || errorexit "Can't commit the changes for ${UAQSRTOOLS_COMMIT_MSG}"
git push "$UAQSRTOOLS_TARGET_REMOTE" "$UAQSRTOOLS_TARGET_BRANCH" \
  || errorexit "Failed to push the changes back to the target ${UAQSRTOOLS_TARGET_REMOTE}"

normalexit "Finished syncing the changes"
